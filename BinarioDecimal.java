/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lenguajeinterfaz;

import java.util.Scanner;

/**
 *
 * @author kary
 */
public class BinarioDecimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String binario;
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese el numero binario a convetir a decimal:");
        binario=leer.next();
        
        long nbinario = Long.parseLong(binario); 
        long ndecimal = 0; 
        int contador = 1; 
        long auxdecimal; 
        
        while (nbinario > 0){
            auxdecimal = nbinario % 2; 
            ndecimal = ndecimal + auxdecimal * contador;
            nbinario = nbinario / 10; 
            contador = contador * 2;
        }
        System.out.println("El numero binario "+binario+" en decimal es: " +ndecimal);
    }
    }
    

