/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lenguajeinterfaz;

import java.util.Scanner;

/**
 *
 * @author kary
 */
public class SumaBinaria {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        String nbinario1;
        String nbinario2;
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese el primer numero binario a sumar:");
        nbinario1=leer.next();
        System.out.println("Ingrese el segundo numero binario a sumar:");
        nbinario2=leer.next();
        
        int num1 = Integer.parseInt(nbinario1, 2);
        int num2 = Integer.parseInt(nbinario2, 2);
        int res = num1 + num2;
        String resultado = Integer.toString(res, 2);
        System.out.println("El resultado de la suma realizada es: " +resultado);
    }
    
}
